const express = require("express");
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
//const {AppServerModuleNgFactory, LAZY_MODULE_MAP} = require('./dist/server/main');
const userFiles = './user_upload/';
const fs = require('fs');

const app = express();
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));


// parse requests of content-type - application/json
app.use(bodyParser.json({limit: '50mb'}));  //related to file upload data

var cors = require('cors');
app.use(cors());

app.use((error, request, response, next) => {
  if (error !== null) {
    return response.json({ 'invalid': 'json' });
  }
  return next();
});

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Outwork sales application api service calls base url." });
});

require("./app/routes/user.routes.js")(app);
require("./app/routes/template.routes.js")(app);
require("./app/routes/contacts.routes.js")(app);

// set port, listen for requests
const PORT =  process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
