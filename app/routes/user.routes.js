module.exports = app => {
  const authJwt  = require("../middlewares/authJwt.js");
  const users = require("../controllers/user.controller.js");
  const upload = require("../controllers/import.controller.js");

   // import file upload
  app.post("/api/upload", upload.importAccounts);  //[authJwt.verifyToken], 

  // Create a new User
  app.post("/api/users", users.create); //,[authJwt.verifyToken]

  // Retrieve all Customers
  app.get("/api/users",[authJwt.verifyToken], users.findAll); // [authJwt.verifyToken],

  // Retrieve a single User with userId
  app.get("/api/users/:userId",[authJwt.verifyToken], users.findOne);

  // Update a User with userId
  app.put("/api/users/:userId",[authJwt.verifyToken], users.update);

  // Delete a User with userId
  app.delete("/api/users/:userId",[authJwt.verifyToken], users.delete);

  // Create a new User
  app.delete("/api/users", users.deleteAll);
  
  // Login a User
  app.post("/admin/v1/user/login", users.login);
  
};
