module.exports = app =>{
const authJwt  = require("../middlewares/authJwt.js");
const templates = require("../controllers/template.controller");

 // Retrieve all Customers
 app.get("/api/templates",[authJwt.verifyToken], templates.findAll); // [authJwt.verifyToken],
};

 