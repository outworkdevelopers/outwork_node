module.exports = app =>{
const authJwt = require("../middlewares/authJwt.js");
const Contacts = require("../controllers/contacts.controller");

app.get("/api/contacts",[authJwt.verifyToken],Contacts.getIntervalContacts);

app.post("/api/contacts/sp",[authJwt.verifyToken],Contacts.getSPContacts);

app.post("/api/contacts/contactsinfo",[authJwt.verifyToken],Contacts.getContactsByCustomquery);

};