const UserLoginResponse ={
    UserToken:String,
    CompanyInfo:String
};


const CompanyResponse = {    
    CompanyId:String,
    CompanyIntId:BigInt,
    CompanyName:String,
    CompanyCode:String,
    CompanyObjectType:String,
    CompanyIsDeleted:String,
    OwnerId:String,
    CompanyTitle:String,
    CompanyType:String,
    CompanyIcon:String,
    CompanyImage:String,
    CompanyDisplayName:String,
    CompanyInternalName:String,
    CompanyStatus:String,
    CreatedDate:String,
    ModifiedDate:String,
    UnreadCount:String,
    Featured:String,
    IsproductAvailable:String,
    IshealthcareAvailable:String,
    IstaskAvailable:String,
    IsorderproductAvailable:String,
    Apptype:String
};