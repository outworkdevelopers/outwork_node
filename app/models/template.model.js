const sql = require("./db.js");


// template constructor
const Template = function (templateObj) {
    sql.query(`SELECT MD5(UUID()) AS TemplateId`, (err, res) => {
        if (res[0].TemplateId) { this.templateid = res[0].TemplateId; }
        this.groupId = templateObj.groupId;
        this.templateName = templateObj.templateName;
        this.mailType = templateObj.mailType;

    });
}


Template.getTemplates = (params,result) => {
    sql.query("SELECT * FROM template WHERE companyintid="+params.companyintid, (err, res) => {
        if (err) {
            console.log("error in get templates:", err);
            result(null, err);
            return;
        }
        result(null, res);
    });
};

module.exports = Template;