const sql = require("./db.js");

// contacts constructor
const Contacts = function (contactsObj) {
    
}

// get contacts periodically
Contacts.getPeriodicContacts = result=> {
   sql.query("SELECT * FROM contacts WHERE companyintid=21 LIMIT 5;",(err,res) =>{
    if (err) {
        console.log("error in get contacts:", err);
        result(null, err);
        return;
    }
    result(null, res);
   });
  // sql.end();
};

// get contacts by custom query
Contacts.getContactsByCustomQuery = (userInfo,inputBody,result) => {
    console.log("custom query hitted");
    let CompanyIntId = userInfo.companyintid;
    let limit = inputBody.limit;
    let startValue = inputBody.startvalue;
    let sortcol = inputBody.sortcol;
    let sortdir = inputBody.sortdir;
    let searchcol = inputBody.searchcol;
    let searchkey = inputBody.searchkey;
    let searchtype = inputBody.searchtype;
    let globalsearch = inputBody.globalsearch;
    let periodcol = inputBody.periodcol;
    let periodtype = inputBody.periodtype;
    //  let baseQuery = "SELECT SQL_CALC_FOUND_ROWS at.accountname AS accountname,at.industry AS industry,ct.*,ass.assignedto, CONCAT(us.`firstname`,' ',us.`lastname`) AS fullname FROM contacts ct LEFT JOIN" +
    //  " accounts `at` ON ct.accountintid = at.id LEFT JOIN assignees ass ON ct.id = ass.objectid LEFT JOIN users us ON us.`id` = ass.`assignedto` WHERE ct.companyintid = " + CompanyIntId + " AND" +
    //  " ct.isdeleted = 0 AND ass.objecttype = 'contact'";
     //let globalSearchColsQuery = "CONCAT_WS(LOWER(at.accountname),LOWER(at.industry), LOWER(ct.contactname), LOWER(ct.stage), LOWER(CONCAT(us.`firstname`,' ',us.`lastname`)))";
    // baseQuery += " LIMIT " + startValue + "," + limit + ";";
     
    // baseQuery += "SELECT FOUND_ROWS() AS COUNT;";             
    // let finalQuery = baseQuery + totalQuery;
    let params = CompanyIntId+","+startValue+","+limit+",'"+sortcol+"','"+sortdir+"','"+searchcol+"','"+
    searchkey+"','"+searchtype+"','"+globalsearch+"','"+periodcol+"','"+periodtype+"'";
     let customSp =`CALL or_contacts_getcontacts_serversideprocessing(`+params+`)`;
     sql.query(customSp, (err, res) => {
         if (err) {
             console.log("error in get contacts from sp:", err);
             result(null, err);
             return;
         }
         result(null, res);
     });
    // console.log("final Query: "+finalQuery);
    // sql.end();
 };

// get contacts by sp
Contacts.getContactsBySp = (userInfo,inputBody,result) => {
   let companyintid = userInfo.companyintid;
   let recordLimit = inputBody.limit;
    let params = companyintid+","+recordLimit;
    let getContactsSP = "CALL ow_test2_sp("+params+")";
    sql.query(getContactsSP, (err, res,fields) => {
        if (err) {
            console.log("error in get contacts from sp:", err);
            result(null, err);
            return;
        }
        result(null, res[0]);
    });
   // sql.end();
};

module.exports = Contacts;