const sql = require("./db.js");
var dateTime = require('node-datetime');
var bcrypt = require("bcryptjs");
var salt = bcrypt.genSaltSync(10);
var dt = dateTime.create();
var formatted = dt.format('Y-m-d H:M:S');
var userid = '';

// constructor
const Customer = function(user) {
    sql.query(`SELECT MD5(UUID()) AS userId`, (err, res) => {
    if (res[0].userId) { this.userid =  res[0].userId; }
    bcrypt.hash('password', salt, (err, hash) => { 
      this.password = hash;
    });
    this.email= user.email;
    this.firstname= user.firstname;
    this.lastname=user.lastname;
    this.mobilenumber=user.mobilenumber;
    this.ObjectType=user.ObjectType;
    this.loginid=user.loginid;
    this.status= user.status;
    this.createddate = formatted;
  });
};

Customer.create = (newUser, result) => { 
  sql.query("INSERT INTO users SET ?", newUser, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    
    console.log("created user: ", { id: res.insertId, ...newUser });
    result(null, { id: res.insertId, ...newUser });
  });
};


Customer.getUUID = (result) => {
  sql.query(`SELECT MD5(UUID()) AS userId`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      newuserId =  res[0].userId;
      return;
    }

    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

Customer.findById = (userId, result) => {
  sql.query(`SELECT * FROM users WHERE id = ${userId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

Customer.getAll = result => {
  sql.query("SELECT * FROM users", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    //console.log("users: ", res);
    result(null, res);
  });
};

Customer.updateById = (id, user, result) => {
  sql.query(
    "UPDATE users SET email = ?, name = ?, active = ? WHERE id = ?",
    [user.email, user.name, user.active, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found Customer with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated user: ", { id: id, ...user });
      result(null, { id: id, ...user });
    }
  );
};

Customer.remove = (id, result) => {
  sql.query("DELETE FROM users WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted user with id: ", id);
    result(null, res);
  });
};

Customer.removeAll = result => {
  sql.query("DELETE FROM users", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} users`);
    result(null, res);
  });
};



Customer.findByEmail = (email, result) => {
  sql.query(`SELECT * FROM users WHERE email ='${email}'`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

module.exports = Customer;
