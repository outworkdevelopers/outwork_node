const User = require("../models/user.model.js");
const CompanyModel = require("../models/company.model");
const responseObj = require("../middlewares/response.js");
const userResponseObj = require("../middlewares/user.js");
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
var privateKey = require("../config/auth.config");

var responseObject = {};

// Create and Save a new User
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }
  
  // Create a User
  const user = new User({
    email: req.body.Email,
    firstname: req.body.FirstName,
    lastname:req.body.LastName,
    mobilenumber:req.body.MobileNumber,
    ObjectType:req.body.ObjectType,
    loginid:req.body.LoginId,
    status: 'ACTIVE'
  });

  // Save User in the database
  User.create(user, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the User."
      });
    else res.send(data);
  });
};

// Retrieve all Customers from the database.
exports.findAll = (req, res) => {  
  User.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving customers."
      });
    else res.send(data);
  });
};

// Find a single User with a userId
exports.findOne = (req, res) => {
  console.log(req.params);
  User.findById(req.params.userId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found User with id ${req.params.userId}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving User with id " + req.params.userId
        });
      }
    } else res.send(data);
  });
};

// Update a User identified by the userId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  User.updateById(
    req.params.userId,
    new User(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found User with id ${req.params.userId}.`
          });
        } else {
          res.status(500).send({
            message: "Error updating User with id " + req.params.userId
          });
        }
      } else res.send(data);
    }
  );
};

// Delete a User with the specified userId in the request
exports.delete = (req, res) => {
  User.remove(req.params.userId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found User with id ${req.params.userId}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete User with id " + req.params.userId
        });
      }
    } else res.send({ message: `User was deleted successfully!` });
  });
};

// Delete all Customers from the database.
exports.deleteAll = (req, res) => {
  User.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all customers."
      });
    else res.send({ message: `All Customers were deleted successfully!` });
  });
};


//User Login
exports.login = (req, res) => {
  const bodyEmail = req.body.loginId;
  var companyInfoData = [];
  
  User.findByEmail(bodyEmail, (err, data) =>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found User with email ${bodyEmail}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving User with email " + bodyEmail
      }); 
      }
    } else {      
      var passwordIsValid = bcrypt.compareSync(req.body.password, data.password);
      console.log("body password: "+req.body.password);
      console.log("db password :"+data.password);
      // if (!passwordIsValid) {
      //   //   return res.status(401).json({accessToken: null, message: "Invalid Password!"});
      //   return res.status(200).json({accessToken: null, message: "Invalid Password!", Code:'SUC-301'});
      // }
      
      var token = jwt.sign({ id: data.id, email:data.email,companyintid:data.companyintid }, privateKey.secret, {expiresIn:"1h"}); 
      if(res.status(200)){       
        CompanyModel.getCompanyInfo(data.userid,(err,data)=>{          
              if(err){
                res.send({
                  message:"Error in fetching company info..!"
                });
              }
              if(res.status(200)){
              console.log("call back data :"+JSON.stringify(data));
               data.forEach(element => {
                 console.log("element items:"+ JSON.stringify(element));
                 companyInfoData.push(element);
               });
               //responseObj.companyInfo = companyInfoData;
               console.log("company info list 1"+ JSON.stringify(companyInfoData));
               responseObj.Code = "SUC-200";
               responseObj.Status="Success";
               responseObj.Data = {
                 Id: data.id,
                 UserId: data.userid,
                 IntUserId:data.id,
                 ObjectType:data.objectType,
                 Name:data.firstname+" "+data.lastname,
                 FirstName:data.firstname,
                 LastName:data.lastname,
                 IsEmailConfigured:data.isemailconfigured,
                 LoginId:data.loginid,
                 companyIntId: data.companyintid,
                 Email: data.email,
                 UserToken: token,
                 companyInfo:JSON.stringify(companyInfoData)       
               };
               responseObj.Message = "Login Information";
               responseObj.Description="Login Information";
               responseObj.HelpUrl="";
               responseObj.Data = JSON.stringify(responseObj.Data);
               res.send(responseObj);               
              }              
        });

      // res.json({
      //     Code:"SUC-200",
      //     Status:"Success",
      //     Data:JSON.stringify({
      //       id: data.id,
      //       userid: data.userid,
      //       role:data.objectType,
      //       companyIntId: data.companyintid,
      //       email: data.email,
      //       accessToken: token
      //     }),
      //     Message:"Login Information",
      //     Description:"Login Information",
      //     HelpUrl:""
      //   });
        
      }      
     
        // res.status(200).json({
        //   id: data.id,
        //   userid: data.userid,
        //   role:data.objectType,
        //   companyIntId: data.companyintid,
        //   email: data.email,
        //   accessToken: token
        // });
    }
  });
};