const ContactsModel = require("../models/contacts.model.js");


// Get Contacts 
exports.getIntervalContacts = (req, res) => {
    ContactsModel.getPeriodicContacts((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving contacts."
            });
        else res.send(data);

    });
};

// Get contacts from stored procedure
exports.getSPContacts = (req, res) => {  
    let utokenDetails = req.userData;
    let inputBody = req.body;    
    ContactsModel.getContactsBySp(utokenDetails,inputBody,(err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurrred while retrieving contacts from stored procedure"
            });
        else res.send(data);
    });
};

// Get contacts from custom query
exports.getContactsByCustomquery = (req, res) => {  
    let utokenDetails = req.userData;
    let inputBody = req.body;    
    ContactsModel.getContactsByCustomQuery(utokenDetails,inputBody,(err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurrred while retrieving contacts from stored procedure"
            });
        else res.send(data);
    });
};